# README #

### Objet du prototype ###

Ce prototype est une étude visant à tester et valider fonctionnellement les solutions techniques de players video HTML5.


### Fonctionnalités couvertes ###

* Chargement de fichiers mp4 HD 720p en streaming => OK
* Insertion de cuepoints => OK
* Création de playlist de cuepoints intéractive => OK
* Chargement des cuepoints depuis un fichier JSON externe => OK
* Rechargement dynamique des cuepoints et de leur liste => OK
* Lecture séquentielle des évènements => OK
* Options de ralenti et d'accéléré => OK
