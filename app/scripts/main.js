$(document).ready(function () {
    // namespace
    var ts = {}

    ts.cuepoints = [
        {
            "time": 2,
            "type": "start"
        },
        {
            "time": 5,
            "type": "end"
        },
        {
            "time": 15,
            "type": "start"
        },
        {
            "time": 18,
            "type": "end"
        },
        {
            "time": 24,
            "type": "start"
        },
        {
            "time": 28,
            "type": "end"
        }
    ]

    // Flowplayer instanciation
    $(".flowplayer-video").flowplayer({cuepoints: [], generate_cuepoints: 1})
        .bind("cuepoint", function(e, api, cuepoint) {
            console.info(e.type, cuepoint.time);
            ts.handle_cuepoint(e, api, cuepoint);
    }).bind("ready", function () {
            ts.generate_cuepoints(ts.cuepoints,"#player_01");
            ts.generate_cuepoints_playlist(ts.cuepoints,"#player_01");
    });


    // Cuepoint handling
    ts.handle_cuepoint = function(e, api, cuepoint) {
        if (cuepoint.type == "end" && cuepoint.index+1 < ts.cuepoints.length) {

            console.log(ts.cuepoints[cuepoint.index+1].time);
            api.seek(ts.cuepoints[cuepoint.index+1].time);
        }
    }

    // Generate cue points and cue points playlist
    ts.generate_cuepoints = function(data, player_id){
        var $player = $(player_id),
            duration = $player.data('flowplayer').video.duration,
            html = '';

        // update cue points
        $player.data('flowplayer').cuepoints = data;

        // update markup
        $player.find('.fp-cuepoint').remove();

        for (var i=0;i<data.length;i++) {
            var width = (data[i].type == 'start') ? 'width:' + ((data[i+1].time - data[i].time) / duration * 100) + '%;' : '';
            html += '<a class="fp-cuepoint fp-cuepoint' + i + ' ' + data[i].type + '" data-type="'+ data[i].type +'" style="left:' + (data[i].time / duration * 100) + '%;'+ width +'"></a>';
        }

        $player.find('.fp-timeline').append(html);

        console.log($(player_id).data('flowplayer'));
    }

    ts.generate_cuepoints_playlist = function(data, player_id) {
        var $player = $(player_id),
            $list_placeholder = $('.playlist[data-player=' + player_id.replace("#", "") + '] ul'),
            html = '',
            count = 1;

        for (var i=0;i<data.length;i++) {
            if (data[i].type == "start") {
                html += '<li><a href="" class="fp-cuepoint fp-cuepoint' + i + '" data-seek="'+ data[i].time +'">Cue point #' + count + '</a></li>';
                count++;
            }
        }

        $list_placeholder.html(html);
    }

    // Update cue points from json list
    ts.update_cuepoints = function(cuepoints_url, player_id){
        $.ajax({
            url: cuepoints_url,
            type: 'GET',
            dataType: 'json',
            success: function(data){
                ts.cuepoints = data;
                ts.generate_cuepoints(data,player_id);
                ts.generate_cuepoints_playlist(data,player_id.substring(1));
            }
        });
    }

    // Navigate through cue points
    ts.cuepoints_navigate = function(playlist_id, seek){
        var api = $('#' + playlist_id).data('flowplayer');
        api.seek(seek);
        api.play();
    }

    // event binding
    $('.update_cuepoints').bind('click', function(e){
        e.preventDefault();

        ts.update_cuepoints($(e.target).data('cuepoints'), $(e.target).attr('href'));
    })

    $('.slow').bind('click', function(e){
        e.preventDefault();

        $($(e.target).attr('href')).data('flowplayer').speed(0.5);
    })

    $('.normal').bind('click', function(e){
        e.preventDefault();

        $($(e.target).attr('href')).data('flowplayer').speed(1);
    })

    $('.fast').bind('click', function(e){
        e.preventDefault();

        $($(e.target).attr('href')).data('flowplayer').speed(2);
    })

    $('.playlist').bind('click', function(e){
        e.preventDefault();

        var playlist_id = $(e.currentTarget).data('player');
        var seek = $(e.target).data('seek');

        ts.cuepoints_navigate(playlist_id, seek);
    });

});

